<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerT6VFTsc\srcApp_KernelTestDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerT6VFTsc/srcApp_KernelTestDebugContainer.php') {
    touch(__DIR__.'/ContainerT6VFTsc.legacy');

    return;
}

if (!\class_exists(srcApp_KernelTestDebugContainer::class, false)) {
    \class_alias(\ContainerT6VFTsc\srcApp_KernelTestDebugContainer::class, srcApp_KernelTestDebugContainer::class, false);
}

return new \ContainerT6VFTsc\srcApp_KernelTestDebugContainer([
    'container.build_hash' => 'T6VFTsc',
    'container.build_id' => '71c68ee5',
    'container.build_time' => 1623965981,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerT6VFTsc');
