<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Document extends \App\Entity\Document implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_id', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_title', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_content', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_creator', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_topic', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_description', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_creationdate', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_likes', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_feedbacks'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_id', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_title', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_content', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_creator', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_topic', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_description', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_creationdate', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_likes', '' . "\0" . 'App\\Entity\\Document' . "\0" . 'document_feedbacks'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Document $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getDocumentTitle(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentTitle', []);

        return parent::getDocumentTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentTitle(string $document_title): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentTitle', [$document_title]);

        parent::setDocumentTitle($document_title);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentContent(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentContent', []);

        return parent::getDocumentContent();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentContent(string $document_content): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentContent', [$document_content]);

        parent::setDocumentContent($document_content);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentCreator(): ?\App\Entity\User
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentCreator', []);

        return parent::getDocumentCreator();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentCreator(?\App\Entity\User $document_creator): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentCreator', [$document_creator]);

        parent::setDocumentCreator($document_creator);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentTopic(): ?\App\Entity\Topic
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentTopic', []);

        return parent::getDocumentTopic();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentTopic(?\App\Entity\Topic $document_topic): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentTopic', [$document_topic]);

        parent::setDocumentTopic($document_topic);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentDescription(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentDescription', []);

        return parent::getDocumentDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentDescription(string $document_description): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentDescription', [$document_description]);

        parent::setDocumentDescription($document_description);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentCreationdate(): \DateTime
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentCreationdate', []);

        return parent::getDocumentCreationdate();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentCreationdate(\DateTime $document_creationdate): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentCreationdate', [$document_creationdate]);

        parent::setDocumentCreationdate($document_creationdate);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentLikes()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentLikes', []);

        return parent::getDocumentLikes();
    }

    /**
     * {@inheritDoc}
     */
    public function setDocumentLikes($document_likes): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDocumentLikes', [$document_likes]);

        parent::setDocumentLikes($document_likes);
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentId(): int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentId', []);

        return parent::getDocumentId();
    }

    /**
     * {@inheritDoc}
     */
    public function getDocumentFeedbacks()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDocumentFeedbacks', []);

        return parent::getDocumentFeedbacks();
    }

}
