<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/register' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\SecurityController::registerAction'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::loginAction'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logoutAction'], null, null, null, false, false, null]],
        '/protected' => [[['_route' => 'protected_content', '_controller' => 'App\\Controller\\SecurityController::promoteUserAction'], null, null, null, false, false, null]],
        '/wiki/topiclist' => [[['_route' => 'topiclist', '_controller' => 'App\\Controller\\WikiController::allTopicListAction'], null, null, null, false, false, null]],
        '/wiki/documents' => [[['_route' => 'documentlist', '_controller' => 'App\\Controller\\WikiController::listAllDocumentsAction'], null, null, null, false, false, null]],
        '/wiki/mydocuments' => [[['_route' => 'mydocuments', '_controller' => 'App\\Controller\\WikiController::listUserDocumentsAction'], null, null, null, false, false, null]],
        '/security/allusers' => [[['_route' => 'allusers', '_controller' => 'App\\Controller\\WikiController::listUsersAction'], null, null, null, false, false, null]],
        '/wiki/addtopic' => [[['_route' => 'addtopic', '_controller' => 'App\\Controller\\WikiController::addTopicAction'], null, null, null, false, false, null]],
        '/wiki/adddocument' => [[['_route' => 'adddocument', '_controller' => 'App\\Controller\\WikiController::addDocumentAction'], null, null, null, false, false, null]],
        '/wiki/statistics' => [[['_route' => 'statistics', '_controller' => 'App\\Controller\\WikiController::mostLikedTopicAction'], null, null, null, false, false, null]],
        '/wiki/mpf' => [[['_route' => 'mpf', '_controller' => 'App\\Controller\\WikiController::mostPopularFeedbacksAction'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/wiki/(?'
                    .'|d(?'
                        .'|elete(?'
                            .'|user/(\\d+)(*:200)'
                            .'|document/(\\d+)(*:222)'
                        .')'
                        .'|ocument/(?'
                            .'|(\\d+)/feedback/([^/]++)/upvote(*:272)'
                            .'|(\\d+)/feedback/([^/]++)/downvote(*:312)'
                            .'|(\\d+)/deletefeedback/(\\d+)(*:346)'
                        .')'
                    .')'
                    .'|topic/(?'
                        .'|(\\d+)(*:370)'
                        .'|(\\d+)/delete(*:390)'
                    .')'
                    .'|read/(\\d+)(*:409)'
                    .'|feedback/(\\d+)(*:431)'
                    .'|edit/(?'
                        .'|user/(\\d+)(*:457)'
                        .'|topic/(\\d+)(*:476)'
                        .'|feedback/(\\d+)(*:498)'
                    .')'
                    .'|like/document/(\\d+)(*:526)'
                .')'
                .'|/user/(\\d+)/changerole(*:557)'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        200 => [[['_route' => 'deleteuser', '_controller' => 'App\\Controller\\SecurityController::deleteUserAction'], ['user_id'], null, null, false, true, null]],
        222 => [[['_route' => 'deletedocument', '_controller' => 'App\\Controller\\WikiController::deleteDocumentAction'], ['document_id'], null, null, false, true, null]],
        272 => [[['_route' => 'upvotedocument', '_controller' => 'App\\Controller\\WikiController::upVoteFeedbackAction'], ['document_id', 'feedback_id'], null, null, false, false, null]],
        312 => [[['_route' => 'downvotedocument', '_controller' => 'App\\Controller\\WikiController::downVoteFeedbackAction'], ['document_id', 'feedback_id'], null, null, false, false, null]],
        346 => [[['_route' => 'deletefeedback', '_controller' => 'App\\Controller\\WikiController::deleteFeedbackAction'], ['document_id', 'feedback_id'], null, null, false, true, null]],
        370 => [[['_route' => 'showtopic', '_controller' => 'App\\Controller\\WikiController::listTopicAction'], ['topic_id'], null, null, false, true, null]],
        390 => [[['_route' => 'deletetopic', '_controller' => 'App\\Controller\\WikiController::deleteTopicAction'], ['topic_id'], null, null, false, false, null]],
        409 => [[['_route' => 'readdocument', '_controller' => 'App\\Controller\\WikiController::readDocumentAction'], ['document_id'], null, null, false, true, null]],
        431 => [[['_route' => 'feedback', '_controller' => 'App\\Controller\\WikiController::addFeedbackAction'], ['document_id'], null, null, false, true, null]],
        457 => [[['_route' => 'edituser', '_controller' => 'App\\Controller\\WikiController::editUserAction'], ['user_id'], null, null, false, true, null]],
        476 => [[['_route' => 'editdocument', '_controller' => 'App\\Controller\\WikiController::editTopicAction'], ['topic_id'], null, null, false, true, null]],
        498 => [[['_route' => 'editfeedback', '_controller' => 'App\\Controller\\WikiController::editFeedbackAction'], ['feedback_id'], null, null, false, true, null]],
        526 => [[['_route' => 'likedocument', '_controller' => 'App\\Controller\\WikiController::likeDocument'], ['document_id'], null, null, false, true, null]],
        557 => [
            [['_route' => 'changerole', '_controller' => 'App\\Controller\\WikiController::changeRole'], ['user_id'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
