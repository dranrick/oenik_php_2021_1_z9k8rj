<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* users.html.twig */
class __TwigTemplate_b1df17635e3f114f74fb02f80933bd5849622c25547cd3246e4f113af8bf0d7b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "users.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "users.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "users.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <table class=\"table table-bordered\">
        <thead>
        <tr>
            <th scope=\"col\">User id</th>
            <th scope=\"col\">User fullname</th>
            <th scope=\"col\">User nickname</th>
            <th scope=\"col\">User permission(s)</th>
            <th scope=\"col\">Creation date</th>
            <th scope=\"col\">Action</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new RuntimeError('Variable "users" does not exist.', 18, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 19
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, $context["element"], "getUserId", [], "any", false, false, false, 19) != twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 19, $this->source); })()), "getUserId", [], "any", false, false, false, 19))) {
                // line 20
                echo "                <tr>
                    <th scope=\"row\">";
                // line 21
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getUserId", [], "any", false, false, false, 21), "html", null, true);
                echo "</th>
                    <td>";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getUserFullname", [], "any", false, false, false, 22), "html", null, true);
                echo "</td>
                    <td>";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getUsername", [], "any", false, false, false, 23), "html", null, true);
                echo "</td>
                    <td>";
                // line 24
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["element"], "getRoles", [], "any", false, false, false, 24), 0, [], "array", false, false, false, 24), "html", null, true);
                echo "</td>
                    <td>";
                // line 25
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["element"], "getUserDate", [], "any", false, false, false, 25), "m/d/Y"), "html", null, true);
                echo "</td>

                    <td>
                        <a style=\"margin-right: 5px\" href=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("deleteuser", ["user_id" => twig_get_attribute($this->env, $this->source, $context["element"], "getUserId", [], "any", false, false, false, 28)]), "html", null, true);
                echo "\">Delete</a>
                    </td>

                </tr>
            ";
            }
            // line 33
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "
        </tbody>
    </table>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 34,  122 => 33,  114 => 28,  108 => 25,  104 => 24,  100 => 23,  96 => 22,  92 => 21,  89 => 20,  86 => 19,  82 => 18,  68 => 6,  58 => 5,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{# @var users \\App\\Entity\\User[] #}


{% block body %}
    <table class=\"table table-bordered\">
        <thead>
        <tr>
            <th scope=\"col\">User id</th>
            <th scope=\"col\">User fullname</th>
            <th scope=\"col\">User nickname</th>
            <th scope=\"col\">User permission(s)</th>
            <th scope=\"col\">Creation date</th>
            <th scope=\"col\">Action</th>
        </tr>
        </thead>
        <tbody>
        {% for element in users %}
            {% if(element.getUserId != user.getUserId) %}
                <tr>
                    <th scope=\"row\">{{ element.getUserId }}</th>
                    <td>{{ element.getUserFullname }}</td>
                    <td>{{ element.getUsername }}</td>
                    <td>{{ element.getRoles[0] }}</td>
                    <td>{{ element.getUserDate|date(\"m/d/Y\") }}</td>

                    <td>
                        <a style=\"margin-right: 5px\" href=\"{{ path('deleteuser', { 'user_id': element.getUserId }) }}\">Delete</a>
                    </td>

                </tr>
            {% endif %}
        {% endfor %}

        </tbody>
    </table>

{% endblock %}
", "users.html.twig", "D:\\Akos\\OE\\6 felev\\Php\\ff_git\\oenik_php_2021_1_z9k8rj\\templates\\users.html.twig");
    }
}
