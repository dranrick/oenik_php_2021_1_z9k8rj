<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wiki/topic.html.twig */
class __TwigTemplate_932e7febb8deab748407b603fe643da668bbeb8a425fcfd0c0f0761e26cd12a8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "wiki/topic.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "wiki/topic.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "wiki/topic.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        // line 5
        echo "    <div class=\"container-fluid\">
        <div class=\"row\">
            ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["documents"]) || array_key_exists("documents", $context) ? $context["documents"] : (function () { throw new RuntimeError('Variable "documents" does not exist.', 7, $this->source); })()), "getTopicDocuments", [], "any", false, false, false, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["document"]) {
            // line 8
            echo "                <div class=\"card\" style=\"width: 18rem;\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentTitle", [], "any", false, false, false, 10), "html", null, true);
            echo "</h5>
                    </div>
                    <ul class=\"list-group list-group-flush\">
                        <li class=\"list-group-item\">Description: ";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentDescription", [], "any", false, false, false, 13), "html", null, true);
            echo "</li>
                        <li class=\"list-group-item\">Creator: ";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentCreator", [], "any", false, false, false, 14), "getUsername", [], "any", false, false, false, 14), "html", null, true);
            echo "</li>
                        <li class=\"list-group-item\">Content: ";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentContent", [], "any", false, false, false, 15), "html", null, true);
            echo "</li>
                        <li class=\"list-group-item\">Uploaded date: ";
            // line 16
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentCreationdate", [], "any", false, false, false, 16), "m/d/Y"), "html", null, true);
            echo "</li>
                        <li class=\"list-group-item\">Likes: ";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentLikes", [], "any", false, false, false, 17), "html", null, true);
            echo "</li>
                    </ul>
                    <div class=\"card-body\">
                        <a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("readdocument", ["document_id" => twig_get_attribute($this->env, $this->source, $context["document"], "getDocumentId", [], "any", false, false, false, 20)]), "html", null, true);
            echo "\" class=\"card-link\">Read document</a>
                    </div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['document'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        </div>
    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "wiki/topic.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 24,  110 => 20,  104 => 17,  100 => 16,  96 => 15,  92 => 14,  88 => 13,  82 => 10,  78 => 8,  74 => 7,  70 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    {# @var documents \\App\\Entity\\Topic #}
    <div class=\"container-fluid\">
        <div class=\"row\">
            {% for document in documents.getTopicDocuments %}
                <div class=\"card\" style=\"width: 18rem;\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">{{ document.getDocumentTitle }}</h5>
                    </div>
                    <ul class=\"list-group list-group-flush\">
                        <li class=\"list-group-item\">Description: {{ document.getDocumentDescription}}</li>
                        <li class=\"list-group-item\">Creator: {{ document.getDocumentCreator.getUsername }}</li>
                        <li class=\"list-group-item\">Content: {{ document.getDocumentContent }}</li>
                        <li class=\"list-group-item\">Uploaded date: {{ document.getDocumentCreationdate|date(\"m/d/Y\") }}</li>
                        <li class=\"list-group-item\">Likes: {{ document.getDocumentLikes }}</li>
                    </ul>
                    <div class=\"card-body\">
                        <a href=\"{{ path('readdocument', { 'document_id': document.getDocumentId }) }}\" class=\"card-link\">Read document</a>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>

{% endblock %}", "wiki/topic.html.twig", "D:\\Akos\\OE\\6 felev\\Php\\ff_git\\oenik_php_2021_1_z9k8rj\\templates\\wiki\\topic.html.twig");
    }
}
