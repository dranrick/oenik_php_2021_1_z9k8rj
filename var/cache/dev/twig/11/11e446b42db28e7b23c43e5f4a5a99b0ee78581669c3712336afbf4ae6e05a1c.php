<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* wiki/document.html.twig */
class __TwigTemplate_2bdc7a5617ba752ff1ca6177a919ec6cee80df08d792af0ebf4347f045600fbe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "wiki/document.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "wiki/document.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "wiki/document.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<section class=\"mb-5\">
    <div class=\"row\">
        <div class=\"col-md-6 mb-4 mb-md-0\">
        </div>
        <div class=\"col-md-6\">

    <h5 style=\"text-align: left; color: cornflowerblue\"><strong>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 12, $this->source); })()), "getDocumentTitle", [], "any", false, false, false, 12), "html", null, true);
        echo "</strong></h5>
            <p class=\"mb-2 text-muted text-uppercase small\">";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 13, $this->source); })()), "getDocumentTopic", [], "any", false, false, false, 13), "getTopicName", [], "any", false, false, false, 13), "html", null, true);
        echo "</p>

            <p class=\"pt-1\">Description: ";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 15, $this->source); })()), "getDocumentDescription", [], "any", false, false, false, 15), "html", null, true);
        echo "</p>
            <div class=\"table-responsive\">
                <table class=\"table table-sm table-borderless mb-0\">
                    <tbody>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Content</strong></th>
                        <td><textarea style=\"size: B4\">";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 21, $this->source); })()), "getDocumentContent", [], "any", false, false, false, 21), "html", null, true);
        echo "</textarea></td>
                    </tr>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Uploader</strong></th>
                        <td>";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 25, $this->source); })()), "getDocumentCreator", [], "any", false, false, false, 25), "getUsername", [], "any", false, false, false, 25), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Upload date</strong></th>
                        <td>";
        // line 29
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 29, $this->source); })()), "getDocumentCreationdate", [], "any", false, false, false, 29), "m/d/Y"), "html", null, true);
        echo "</td>
                        <span class=\"mr-1\">Likes: ";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 30, $this->source); })()), "getDocumentLikes", [], "any", false, false, false, 30), "html", null, true);
        echo "</span>
                    </tr>
                    </tbody>
                </table>
            </div>
    <br>
    <h5>Feedbacks</h5>
    <hr>
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["document"]) || array_key_exists("document", $context) ? $context["document"] : (function () { throw new RuntimeError('Variable "document" does not exist.', 38, $this->source); })()), "getDocumentFeedbacks", [], "any", false, false, false, 38));
        foreach ($context['_seq'] as $context["_key"] => $context["feedback"]) {
            // line 39
            echo "                <p>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["feedback"], "getFeedbackAuthor", [], "any", false, false, false, 39), "getUsername", [], "any", false, false, false, 39), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["feedback"], "getFeedbacktext", [], "any", false, false, false, 39), "html", null, true);
            echo "</p>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feedback'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "            <div style=\"width: 400px;padding: 20px\">
                ";
        // line 42
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 42, $this->source); })()), [0 => "bootstrap_4_layout.html.twig"], true);
        // line 43
        echo "                ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), 'form_start');
        echo "
                ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), 'widget');
        echo "
                ";
        // line 45
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 45, $this->source); })()), 'form_end');
        echo "
            </div>
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "wiki/document.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 45,  149 => 44,  144 => 43,  142 => 42,  139 => 41,  128 => 39,  124 => 38,  113 => 30,  109 => 29,  102 => 25,  95 => 21,  86 => 15,  81 => 13,  77 => 12,  68 => 5,  58 => 4,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}
{# @var document \\App\\Entity\\Document #}

{% block body %}

<section class=\"mb-5\">
    <div class=\"row\">
        <div class=\"col-md-6 mb-4 mb-md-0\">
        </div>
        <div class=\"col-md-6\">

    <h5 style=\"text-align: left; color: cornflowerblue\"><strong>{{ document.getDocumentTitle }}</strong></h5>
            <p class=\"mb-2 text-muted text-uppercase small\">{{ document.getDocumentTopic.getTopicName }}</p>

            <p class=\"pt-1\">Description: {{ document.getDocumentDescription }}</p>
            <div class=\"table-responsive\">
                <table class=\"table table-sm table-borderless mb-0\">
                    <tbody>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Content</strong></th>
                        <td><textarea style=\"size: B4\">{{ document.getDocumentContent }}</textarea></td>
                    </tr>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Uploader</strong></th>
                        <td>{{ document.getDocumentCreator.getUsername }}</td>
                    </tr>
                    <tr>
                        <th class=\"pl-0 w-25\" scope=\"row\"><strong>Upload date</strong></th>
                        <td>{{ document.getDocumentCreationdate|date(\"m/d/Y\") }}</td>
                        <span class=\"mr-1\">Likes: {{ document.getDocumentLikes }}</span>
                    </tr>
                    </tbody>
                </table>
            </div>
    <br>
    <h5>Feedbacks</h5>
    <hr>
            {% for feedback in document.getDocumentFeedbacks %}
                <p>{{ feedback.getFeedbackAuthor.getUsername }}: {{ feedback.getFeedbacktext }}</p>
            {% endfor %}
            <div style=\"width: 400px;padding: 20px\">
                {% form_theme form 'bootstrap_4_layout.html.twig' %}
                {{ form_start(form) }}
                {{ form_widget(form) }}
                {{ form_end(form) }}
            </div>
            {% endblock %}", "wiki/document.html.twig", "D:\\Akos\\OE\\6 felev\\Php\\ff_git\\oenik_php_2021_1_z9k8rj\\templates\\wiki\\document.html.twig");
    }
}
