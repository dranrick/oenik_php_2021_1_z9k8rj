<?php
// php bin/console doctrine:fixtures:load --no-interaction -vvv
// composer require --dev symfony/phpunit-bridge
// php bin/phpunit

namespace App\Tests\Service;

use App\Entity\Document;
use App\Entity\Topic;
use App\Entity\User;
use App\Service\DocumentService;
use App\Service\FeedbackService;
use App\Service\SecurityService;
use App\Service\TopicService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TopicServiceTest extends WebTestCase
{
    /** @var EntityManagerInterface */
    private static $em;

    /** @var TopicService */
    private static $topicService;

    /** @var DocumentService */
    private static $documentService;

    /** @var FeedbackService */
    private static $feedbackService;

    /** @var SecurityService */
    private static $securityService;

    /** @var Topic */
    private $science_topic;

    public static function setUpBeforeClass(): void
    {
        //exec("php bin/console doctrine:fixtures:load --no-interaction");
        self::bootKernel();
        self::$em = self::$kernel->getContainer()->get('doctrine')->getManager();

        self::$topicService = self::$kernel->getContainer()->get('test.topicservice');
        self::$feedbackService = self::$kernel->getContainer()->get('test.feedbackservice');
        self::$documentService = self::$kernel->getContainer()->get('test.documentservice');
        self::$securityService = self::$kernel->getContainer()->get('test.securityservice');

        self::ensureKernelShutdown();
    }

    public function setUp(): void
    {
        $this->science_topic = self::$em->getRepository(Topic::class)->findOneBy(['topic_name' => 'sciencee']);
    }

    public function testTopicExists()
    {
        $this->assertNotNull($this->science_topic);
    }

    public function testCanFetchAllTopics()
    {
        $topics = self::$topicService->getAllTopics();
        $this->assertEquals(2, count($topics));
    }

    private function createDocument() : Document
    {
        $document = new Document();
        $document->setDocumentTitle("test_doc_title");
        $document->setDocumentLikes(0);
        $document->setDocumentContent("content");
        $document->setDocumentDescription("description");
        $document->setDocumentTopic($this->science_topic);
        return $document;
    }

    private function testDocumentAddition()
    {
        // Arrange

        $title = "test_title";
        $user = new User();
        $description = "desc";
        $content = "content";
        $topic_id = $this->science_topic->getTopicId();

        // Act

        self::$documentService->addDocument($title, $user, $description, $content, $topic_id);

        //Assert

        $docresult = self::$em->getRepository(Document::class)->findOneBy(["title"=>"test_title"]);
        $documents = self::$em->getRepository(Document::class)->findAll();

        $this->assertNotNull($docresult);
        $this->assertEquals(5, count($documents));
    }
}

