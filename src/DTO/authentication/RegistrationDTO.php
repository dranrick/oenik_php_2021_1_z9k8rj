<?php


namespace App\DTO;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationDTO extends DTOBase
{
    /** @var string */
    private $fullName = "";

    /** @var string */
    private $userName = "";

    /** @var string */
    private $clearPassword = "";

    /** @var bool */
    private $gdprAgreed = false;

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     */
    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getClearPassword(): string
    {
        return $this->clearPassword;
    }

    /**
     * @param string $clearPassword
     */
    public function setClearPassword(string $clearPassword): void
    {
        $this->clearPassword = $clearPassword;
    }

    /**
     * @return bool
     */
    public function isGdprAgreed(): bool
    {
        return $this->gdprAgreed;
    }

    /**
     * @param bool $gdprAgreed
     */
    public function setGdprAgreed(bool $gdprAgreed): void
    {
        $this->gdprAgreed = $gdprAgreed;
    }

    public function __construct(FormFactoryInterface $formFactory, Request $request)
    {
        parent::__construct($formFactory, $request);
    }

    public function getForm(): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this); //Data from post to DTO
        $builder->add('fullname', TextType::class, ["required"=>true]);
        $builder->add('username', TextType::class, ["required"=>true]);
        $builder->add('clearPassword', RepeatedType::class, [
            'type' => PasswordType::class, //what to repeat
            'invalid_message' => 'The passwords must match!',
            'required' => true,
            'first_options' => ["label" => "Password"],
            'second_options' => ["label" => "Password again"],
            'constraints' => [
                new NotBlank(["message" => "Password cannot be empty"]),
                new Length([
                    'min' => 8,
                    'max' => 4096,
                    'minMessage' => 'Password length must be minimum {{ limit }} characters.'
                ])
            ]
        ]);

        $builder->add('gdprAgreed', CheckboxType::class, ["constraints"=>[
            new IsTrue(["message" => "You must agree GDPR rules!"])
        ]]); //automatic validation func

        $builder->add('Create account', SubmitType::class);
        return $builder->getForm();

    }


}