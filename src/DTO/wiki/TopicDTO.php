<?php

namespace App\DTO\wiki;

use App\DTO\DTOBase;
use App\Entity\Topic;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class TopicDTO extends DTOBase
{
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    public function __construct(FormFactoryInterface $formFactory, Request $request)
    {
        parent::__construct($formFactory, $request);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getForm(): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('name', TextType::class, ["required"=>true]);
        $builder->add('description', TextType::class, ["required"=>true]);
        $builder->add('Create topic', SubmitType::class);

        return $builder->getForm();
    }

    public function getModificationForm(Topic $topic) : FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('name', TextType::class, ["required"=>true, "data" => $topic->getTopicName()]);
        $builder->add('description', TextType::class, ["required"=>true, "data"=>$this->getDescription()]);
        $builder->add('Create topic', SubmitType::class);

        return $builder->getForm();
    }
}