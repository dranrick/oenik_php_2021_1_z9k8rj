<?php


namespace App\DTO\wiki;

use App\DTO\DTOBase;
use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserDTO extends  DTOBase
{
    /** @var string  */
    private $fullname = "";

    /** @var string  */
    private $username = "";

    /** @var string  */
    private $clearPassword = "";

    public function __construct(FormFactoryInterface $formFactory, Request $request)
    {
        parent::__construct($formFactory, $request);
    }

    /**
     * @return string
     */
    public function getFullname(): string
    {
        return $this->fullname;
    }

    /**
     * @param string $fullname
     */
    public function setFullname(string $fullname): void
    {
        $this->fullname = $fullname;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getClearPassword(): string
    {
        return $this->clearPassword;
    }

    /**
     * @param string $clearPassword
     */
    public function setClearPassword(string $clearPassword): void
    {
        $this->clearPassword = $clearPassword;
    }

    public function getForm(): FormInterface
    {
        // TODO: Implement getForm() method.
    }


    public function getModificationForm(User $userToEdit): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('fullname', TextType::class, ["required"=>true, "data"=>$userToEdit->getUserFullname()]);
        $builder->add('username', TextType::class, ["required"=>true, "data"=>$userToEdit->getUsername()]);

        $builder->add('clearPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'empty_data' => "",
            'required' => false,
            'invalid_message' => 'The passwords must match!',
            'first_options' => ["label" => "Password"],
            'second_options' => ["label" => "Password again"],
            'constraints' => [
                new Length([
                    'min' => 6,
                    'minMessage' => 'Password length minimum {{ limit }} characters',
                    'max' => 4096
                ])
            ]
        ]);

        $builder->add('Change data', SubmitType::class);

        return $builder->getForm();
    }


}