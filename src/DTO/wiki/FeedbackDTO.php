<?php

namespace App\DTO\wiki;

use App\DTO\DTOBase;
use App\Entity\Feedback;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FeedbackDTO extends DTOBase
{

    /** @var string  */
    private $feedback_text;

    public function __construct(FormFactoryInterface $formFactory, Request $request)
    {
        parent::__construct($formFactory, $request);
    }

    /**
     * @return string
     */
    public function getFeedbackText(): ?string
    {
        return $this->feedback_text;
    }

    /**
     * @param string $feedback_text
     */
    public function setFeedbackText(string $feedback_text): void
    {
        $this->feedback_text = $feedback_text;
    }


    public function getForm(): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('feedback_text', TextType::class, ["required"=>true]);
        $builder->add('Send', SubmitType::class);

        return $builder->getForm();
    }

    public function getModificationForm(Feedback $feedback): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('feedback_text', TextType::class, ["required"=>true, "data"=>$feedback->getFeedbackText()]);
        $builder->add('Send', SubmitType::class);

        return $builder->getForm();
    }
}