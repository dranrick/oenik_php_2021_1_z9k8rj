<?php

namespace App\DTO\wiki;

use App\DTO\DTOBase;
use App\Entity\Document;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DocumentDTO extends DTOBase
{
    /** @var string  */
    private $topic;

    /** @var string  */
    private $title;

    /** @var string  */
    private $content;

    /** @var string  */
    private $description;

    /**
     * @return string
     */
    public function getTopic(): ?string
    {
        return $this->topic;
    }

    /**
     * @param string $topic
     */
    public function setTopic(string $topic): void
    {
        $this->topic = $topic;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }



    public function __construct(FormFactoryInterface $formFactory, Request $request)
    {
        parent::__construct($formFactory, $request);
    }


    public function getForm(): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('topic', TextType::class);
        $builder->add('title', TextType::class);
        $builder->add('description', TextType::class);
        $builder->add('content', TextareaType::class);

        $builder->add('SEND', SubmitType::class);

        return $builder->getForm();
    }

    public function getModificationForm(Document $document) : FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('title', TextType::class, ["required"=>true, "data"=>$document->getDocumentTitle()]);
        $builder->add('description', TextType::class, ["required"=>true, "data"=>$document->getDocumentDescription()]);
        $builder->add('content', TextareaType::class, ["required"=>true, "data"=>$document->getDocumentContent()]);

        $builder->add('Edit', SubmitType::class);

        return $builder->getForm();
    }

    public function getUpgradedForm($topics): FormInterface
    {
        $builder = $this->formFactory->createBuilder(FormType::class, $this);

        $builder->add('topic', ChoiceType::class, [
            'choices'  => $topics
        ]);
        $builder->add('title', TextType::class);
        $builder->add('description', TextType::class);
        $builder->add('content', TextareaType::class);

        $builder->add('SEND', SubmitType::class);

        return $builder->getForm();
    }
}