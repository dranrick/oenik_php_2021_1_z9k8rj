<?php

namespace App\Controller;

use App\Entity\Document;
use App\Entity\User;
use App\DTO\LoginDTO;
use Symfony\Component\DependencyInjection\Compiler\ResolveBindingsPass;
use Symfony\Component\Routing\Annotation\Route;
use App\DTO\RegistrationDTO;
use App\Service\SecurityService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @var SecurityService
     */
    private $securityService;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    private $category;

    private $product;

    private $comment;

    public function __construct(SecurityService $securityService, FormFactoryInterface $formFactory)
    {
        $this->securityService = $securityService;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="app_register", path="/register")
     */
    public function registerAction(Request $request) : Response
    {
        $dto = new RegistrationDTO($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->securityService->registerUser($dto->getUserName(), $dto->getFullName(), $dto->getClearPassword());
            return $this->redirectToRoute('app_register');
        }

        return $this->render('security/register.html.twig', ['form'=>$form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $user_id
     * @return Response
     * @Route(name="deleteuser", path="/wiki/deleteuser/{user_id}", requirements={ "user_id": "\d+" })
     */
    public function deleteUserAction(Request $request, int $user_id) : Response
    {
        /** @var User $userToDelete */
        $userToDelete = $this->securityService->getUserById($user_id);

        /** @var User $currentuser */
        $currentuser = $this->getUser();

        if ($currentuser && $currentuser->getRoles() == ["ROLE_ADMIN"] && $currentuser != $userToDelete && $userToDelete->getRoles() != ["ROLE_ADMIN"])
        {
            $this->securityService->deleteUser($user_id);
            $this->addFlash('notice', "User deleted successfully!");
            echo "sikeres";
        }
        else
        {
            echo "sikertelen";
            $this->addFlash('notice', "You have no authorization to proceed this action!");
        }

        return $this->redirectToRoute("app_login");
    }

    /**
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     * @Route(name="app_login", path="/login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils) : Response
    {
        $dto = new LoginDTO($this->formFactory, $request);
        $dto->setUserName($authenticationUtils->getLastUsername());

        return $this->render('security/login.html.twig', [
            'form' => $dto->getForm()->createView(),
            'myUser' => $this->getUser(),
            'authError'=>$authenticationUtils->getLastAuthenticationError()
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="app_logout", path="/logout")
     */
    public function logoutAction(Request $request) : Response
    {
        // Never actually executed => Logout will be done by the framework!!
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="protected_content", path="/protected")
     */
    public function protectedAction(Request $request) : Response
    {
        // if ($this->isGranted("ROLE_ADMIN"))
        $this->denyAccessUnlessGranted("ROLE_ADMIN");
        return new Response("Something TOP SECRET...");
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Response
     * @Route(name="protected_content", path="/protected")
     */
    public function promoteUserAction(Request $request, User $user) : Response
    {

    }
}