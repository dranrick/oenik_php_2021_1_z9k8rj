<?php


namespace App\Controller;

use App\DTO\wiki\DocumentDTO;
use App\DTO\wiki\TopicDTO;
use App\DTO\wiki\FeedbackDTO;
use App\DTO\LoginDTO;
use App\DTO\RegistrationDTO;
use App\DTO\wiki\UserDTO;
use App\Entity\Document;
use App\Entity\Topic;
use App\Entity\User;
use App\Service\DocumentService;
use App\Service\FeedbackService;
use App\Service\TopicService;
use PhpParser\Node\Expr\Array_;
use Symfony\Component\DependencyInjection\Compiler\ResolveBindingsPass;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\SecurityService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class WikiController extends AbstractController
{
    private $securityService;

    private $documentService;

    private $topicService;

    private $feedbackService;

    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory, SecurityService $securityService, TopicService $topicService, DocumentService $documentService, FeedbackService $feedbackService)
    {
        $this->securityService = $securityService;
        $this->formFactory = $formFactory;
        $this->topicService = $topicService;
        $this->documentService = $documentService;
        $this->feedbackService = $feedbackService;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="topiclist", path="/wiki/topiclist")
     */
    public function allTopicListAction(Request $request): Response
    {
        $topics = $this->topicService->getAllTopics();
        return $this->render('wiki/alltopics.html.twig', ["topics" => $topics]);
    }

    /**
     * @param Request $request
     * @param int $topic_id
     * @return Response
     * @Route(name="showtopic", path="/wiki/topic/{topic_id}", requirements={ "topic_id": "\d+" })
     */
    public function listTopicAction(Request $request, int $topic_id) : Response
    {
        $documents = $this->topicService->getTopicById($topic_id);
        return $this->render('wiki/topic.html.twig', ["documents" => $documents]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="documentlist", path="/wiki/documents")
     */
    public function listAllDocumentsAction(Request $request) : Response
    {
        $documents = $this->documentService->getAllDocuments();
        return $this->render('wiki/alldocuments.html.twig', ["documents" => $documents]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="mydocuments", path="/wiki/mydocuments")
     */
    public function listUserDocumentsAction(Request $request) : Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $documents = $this->documentService->getDocumentsByUserId($user->getUserId());

        return $this->render('wiki/topic.html.twig', ['documents' => $documents]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="allusers", path="/security/allusers")
     */
    public function listUsersAction(Request $request) : Response
    {
        $this->denyAccessUnlessGranted(["ROLE_ADMIN"]);

        $users = $this->securityService->getAllUsers();
        $currentUser = $this->getUser();

        return $this->render("security/users.html.twig", ['users' => $users, 'user' => $currentUser]);
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @return Response
     * @Route(name="readdocument", path="/wiki/read/{document_id}", requirements={ "document_id": "\d+" })
     */
    public function readDocumentAction(Request $request, int $document_id): Response
    {
        $actualuser = $this->getUser();
        echo $actualuser->getUserId();

        $documentToRead = $this->documentService->getDocumentById($document_id);

        $dto = new FeedbackDTO($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $actualuser */

            $this->feedbackService->addFeedback($document_id, $actualuser, $dto->getFeedbackText());
            //$this->addFlash('notice', ); ....
            return $this->redirectToRoute('readdocument', [
                'request' => $request,
                'document_id' => $document_id
            ]);
        }

        return $this->render('wiki/document.html.twig', ["document" => $documentToRead, "form" => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="addtopic", path="/wiki/addtopic")
     */
    public function addTopicAction(Request $request): Response
    {
        $dto = new TopicDTO($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->topicService->addTopic($dto->getName(), $dto->getDescription());

            return $this->redirectToRoute('app_login');
        }

        return $this->render('wiki/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="adddocument", path="/wiki/adddocument")
     */
    public function addDocumentAction(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_USER");

        $dto = new DocumentDTO($this->formFactory, $request);

        $names = array();

        /** @var Topic[] $topics */
        $topics = $this->topicService->getAllTopics();
        foreach ($topics as $topic)
        {
            array_push($names, $topic->getTopicName());
        }

        $names = array_flip($names);
        $form = $dto->getUpgradedForm($names);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
            $user = $this->getUser();

            $names = array_flip($names);
            echo $names[0];

            $topic_id_found = $this->topicService->getIdByTitle($names[$dto->getTopic()]);

            $this->documentService->addDocument($dto->getTitle(), $user, $dto->getDescription(), $dto->getContent(), $topic_id_found);
        }

        return $this->render('wiki/add.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @return Response
     * @Route(name="feedback", path="/wiki/feedback/{document_id}", requirements={ "document_id": "\d+" })
     */
    public function addFeedbackAction(Request $request, int $document_id): Response
    {
        $dto = new FeedbackDTO($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var User $user */
            $user = $this->getUser();

            $this->feedbackService->addFeedback($document_id, $user, $dto->getFeedbackText());
            //$this->addFlash('notice', ); ....
            return $this->redirectToRoute('app_register');
        }

        return $this->render('wiki/document.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $user_id
     * @return Response
     * @Route(name="edituser", path="wiki/edit/user/{user_id}", requirements={ "user_id": "\d+" })
     */
    public function editUserAction(Request $request, int $user_id): Response
    {
        $userToEdit = $this->securityService->getUserById($user_id);

        $dto = new UserDTO($this->formFactory, $request);

        $form = $dto->getModificationForm($userToEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->securityService->editUser($user_id,
                $dto->getUsername(), $dto->getFullname(), $dto->getClearPassword());

            return $this->redirectToRoute('allusers');
        }

        return $this->render('security/edituser.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @return Response
     * @Route(name="editdocument", path="wiki/edit/document/{document_id}", requirements={ "document_id": "\d+" })
     */
    public function editDocumentAction(Request $request, int $document_id): Response
    {
        $productToEdit = $this->documentService->getDocumentById($document_id);

        $dto = new DocumentDTO($this->formFactory, $request);
        $form = $dto->getModificationForm($productToEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->documentService->editDocument($document_id,
                $dto->getTitle(), $dto->getDescription(), $dto->getContent());

            return $this->redirectToRoute('readdocument', [
                'request' => $request,
                'document_id' => $document_id
            ]);
        }

        return $this->render('wiki/editdocument.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $topic_id
     * @return Response
     * @Route(name="editdocument", path="wiki/edit/topic/{topic_id}", requirements={ "topic_id": "\d+" })
     */
    public function editTopicAction(Request $request, int $topic_id): Response
    {
        $topicToEdit = $this->topicService->getTopicById($topic_id);

        $dto = new TopicDTO($this->formFactory, $request);
        $form = $dto->getModificationForm($topicToEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $this->topicService->editTopic($topic_id,
                $dto->getName(), $dto->getDescription());

            return $this->redirectToRoute('showtopic', [
                'request' => $request,
                'topic_id' => $topic_id
            ]);
        }

        return $this->render('wiki/editdocument.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $feedback_id
     * @return Response
     * @Route(name="editfeedback", path="wiki/edit/feedback/{feedback_id}", requirements={ "feedback_id": "\d+" })
     */
    public function editFeedbackAction(Request $request, int $feedback_id): Response
    {
        $feedbackToEdit = $this->feedbackService->getFeedbackById($feedback_id);

        /** @var Document $document */
        $document = $feedbackToEdit->getFeedbackDocument();
        $document_id = $document->getDocumentId();

        $dto = new FeedbackDTO($this->formFactory, $request);
        $form = $dto->getModificationForm($feedbackToEdit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->feedbackService->editFeedback($feedback_id,
                $dto->getFeedbackText());

            return $this->redirectToRoute('readdocument', [
                'request' => $request,
                'document_id' => $document_id
            ]);
        }

        return $this->render('wiki/editfeedback.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @return Response
     * @Route(name="likedocument", path="/wiki/like/document/{document_id}", requirements={ "document_id": "\d+" })
     */
    public function likeDocument(Request $request, int $document_id) : Response
    {
        $this->documentService->likeDocument($document_id);

        return $this->redirectToRoute('readdocument', [
            'request' => $request,
            'document_id' => $document_id
        ]);
    }

    /**
     * @param Request $request
     * @param int $feedback_id
     * @param int $document_id
     * @return Response
     * @Route(name="upvotedocument", path="/wiki/document/{document_id}/feedback/{feedback_id}/upvote", requirements={ "feedback_Id": "\d+", "document_id": "\d+"})
     */
    public function upVoteFeedbackAction(Request $request, int $feedback_id, int $document_id) : Response
    {
        echo "eljutott ide a vez";
        $this->feedbackService->voteOnFeedback($feedback_id, 1);

        return $this->redirectToRoute('readdocument', [
            'request' => $request,
            'document_id' => $document_id
        ]);
    }


    /**
     * @param Request $request
     * @param int $feedback_id
     * @param int $document_id
     * @return Response
     * @Route(name="downvotedocument", path="/wiki/document/{document_id}/feedback/{feedback_id}/downvote", requirements={ "feedback_Id": "\d+", "document_id": "\d+"})
     */
    public function downVoteFeedbackAction(Request $request, int $feedback_id, int $document_id) : Response
    {
        $this->feedbackService->voteOnFeedback($feedback_id, 0);

        return $this->redirectToRoute('readdocument', [
            'request' => $request,
            'document_id' => $document_id
        ]);
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @return Response
     * @Route(name="deletedocument", path="/wiki/deletedocument/{document_id}", requirements={ "document_id": "\d+" })
     */
    public function deleteDocumentAction(Request $request, int $document_id) : Response
    {
        /** @var Document $documentToDelete */
        $documentToDelete = $this->documentService->getDocumentById($document_id);

        $user = $this->getUser();
        if ($user && ($user->getRoles() == ["ROLE_ADMIN"] || $documentToDelete->getDocumentCreator() == $user))
        {
            $this->documentService->deleteDocument($document_id);
            $this->addFlash('notice', "Document deleted successfully!");
        }
        else
        {
            $this->addFlash('notice', "You have no authorization to proceed this action!");
        }

        return $this->redirectToRoute("topiclist");
    }

    /**
     * @param Request $request
     * @param int $topic_id
     * @return Response
     * @Route(name="deletetopic", path="wiki/topic/{topic_id}/delete", requirements={ "topic_id": "\d+" })
     */
    public function deleteTopicAction(Request $request, int $topic_id) : Response
    {
        $this->denyAccessUnlessGranted("[ROLE_ADMIN]");

        /** @var Topic $topic */
        $topic = $this->topicService->getTopicById($topic_id);

        $this->topicService->deleteTopic($topic_id);
        return $this->redirectToRoute("topiclist");
    }

    /**
     * @param Request $request
     * @param int $document_id
     * @param int $feedback_id
     * @return Response
     * @Route(name="deletefeedback", path="/wiki/document/{document_id}/deletefeedback/{feedback_id}", requirements={"document_id": "\d+","feedback_id": "\d+" })
     */
    public function deleteFeedbackAction(Request $request, int $document_id, int $feedback_id) : Response
    {
        $this->feedbackService->deleteFeedback($feedback_id);

        return $this->redirectToRoute('readdocument', [
            'request' => $request,
            'document_id' => $document_id
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="statistics", path="/wiki/statistics")
     */
    public function mostLikedTopicAction(Request $request) : Response
    {
        /** @var array $result */
        $result = $this->topicService->mostAvgLikedTopic();

        var_dump($result);
        sort($result);

        return $this->render("wiki/statistics.html.twig", ["result" => $result]);
    }

    /**
     * @param Request $request
     * @param int $threshold
     * @return Response
     * @Route(name="mpf", path="/wiki/mpf")
     */
    public function mostPopularFeedbacksAction(Request $request, int $threshold) : Response
    {
        $mpf = $this->feedbackService->getPopularFeedbacks($threshold);
        return $this->render("wiki/mpf.html.twig", ["result" => $mpf]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route(name="changerole", path="/user/{user_id}/changerole", requirements={ "user_id": "\d+" })
     */
    public function changeRole(Request $request, $user_id) : Response{
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        $this->securityService->changeRole($user_id);

        return $this->redirectToRoute('allusers');
    }
}