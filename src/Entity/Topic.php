<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Topic
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="topics")
 */

class Topic
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $topic_id = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $topic_name = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    private $topic_description = "";

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $topic_creationdate;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $topic_documentCount = 0;

    /**
     * @var Collection|Document[]
     * @ORM\OneToMany(targetEntity="Document", mappedBy="document_topic")
     */
    private $topic_documents;

    public function __construct()
    {
        $this->topic_documents = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getTopicId(): int
    {
        return $this->topic_id;
    }

    /**
     * @return string
     */
    public function getTopicName(): string
    {
        return $this->topic_name;
    }

    /**
     * @param string $topic_name
     */
    public function setTopicName(string $topic_name): void
    {
        $this->topic_name = $topic_name;
    }

    /**
     * @return string
     */
    public function getTopicDescription(): string
    {
        return $this->topic_description;
    }

    /**
     * @param string $topic_description
     */
    public function setTopicDescription(string $topic_description): void
    {
        $this->topic_description = $topic_description;
    }

    /**
     * @return \DateTime
     */
    public function getTopicCreationdate(): \DateTime
    {
        return $this->topic_creationdate;
    }

    /**
     * @param \DateTime $topic_creationdate
     */
    public function setTopicCreationdate(\DateTime $topic_creationdate): void
    {
        $this->topic_creationdate = $topic_creationdate;
    }

    /**
     * @return int
     */
    public function getTopicDocumentCount(): int
    {
        return $this->topic_documentCount;
    }

    /**
     * @param int $topic_documentCount
     */
    public function setTopicDocumentCount(int $topic_documentCount): void
    {
        $this->topic_documentCount = $topic_documentCount;
    }

    /**
     * @return Collection|Document[]
     */
    public function getTopicDocuments()
    {
        return $this->topic_documents;
    }
}