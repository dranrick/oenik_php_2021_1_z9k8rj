<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Feedback
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="feedbacks")
 */

class Feedback
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $feedback_id = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $feedback_text = "";

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $feedback_upvotes = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $feedback_downvotes = 0;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $feedback_date;

    /**
     * @var Document|null
     * @ORM\JoinColumn(name="feedback_document", referencedColumnName="document_id")
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="document_feedbacks")
     */
    private $feedback_document;

    /**
     * @var User|null
     * @ORM\JoinColumn(name="feedback_author", referencedColumnName="user_id")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="user_feedbacks")
     */
    private $feedback_author;

    /**
     * @return int
     */
    public function getFeedbackId(): int
    {
        return $this->feedback_id;
    }

    /**
     * @return string
     */
    public function getFeedbackText(): string
    {
        return $this->feedback_text;
    }

    /**
     * @param string $feedback_text
     */
    public function setFeedbackText(string $feedback_text): void
    {
        $this->feedback_text = $feedback_text;
    }

    /**
     * @return int
     */
    public function getFeedbackUpvotes(): int
    {
        return $this->feedback_upvotes;
    }

    /**
     * @param int $feedback_upvotes
     */
    public function setFeedbackUpvotes(int $feedback_upvotes): void
    {
        $this->feedback_upvotes = $feedback_upvotes;
    }

    /**
     * @return int
     */
    public function getFeedbackDownvotes(): int
    {
        return $this->feedback_downvotes;
    }

    /**
     * @param int $feedback_downvotes
     */
    public function setFeedbackDownvotes(int $feedback_downvotes): void
    {
        $this->feedback_downvotes = $feedback_downvotes;
    }

    /**
     * @return \DateTime
     */
    public function getFeedbackDate(): \DateTime
    {
        return $this->feedback_date;
    }

    /**
     * @param \DateTime $feedback_date
     */
    public function setFeedbackDate(\DateTime $feedback_date): void
    {
        $this->feedback_date = $feedback_date;
    }

    /**
     * @return Document|null
     */
    public function getFeedbackDocument(): ?Document
    {
        return $this->feedback_document;
    }

    /**
     * @param Document|null $feedback_document
     */
    public function setFeedbackDocument(?Document $feedback_document): void
    {
        $this->feedback_document = $feedback_document;
    }

    /**
     * @return User|null
     */
    public function getFeedbackAuthor(): ?User
    {
        return $this->feedback_author;
    }

    /**
     * @param User|null $feedback_author
     */
    public function setFeedbackAuthor(?User $feedback_author): void
    {
        $this->feedback_author = $feedback_author;
    }
}