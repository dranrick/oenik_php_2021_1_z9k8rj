<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="users");
 */

class User implements UserInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $user_id = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $user_fullname = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $user_username = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $user_password = "";

    /**
     * @var Collection|Document[]
     * @ORM\OneToMany(targetEntity="Document", mappedBy="document_creator")
     */
    private $user_documents;

    /**
     * @var Collection|Feedback[]
     * @ORM\OneToMany(targetEntity="Feedback", mappedBy="feedback_author")
     */
    private $user_feedbacks;

    /**
     * @var string[]
     * @ORM\Column(type="json")
     */
    private $user_permissions = array();

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $user_creationdate;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $user_sumUpVotes = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $user_sumDownVotes = 0;

    public function __construct()
    {
        $this->user_documents = new ArrayCollection();
        $this->user_feedbacks = new ArrayCollection();
    }

    public function __toString()
    {
        return "{$this->user_username} {$this->user_fullname}";
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }


    /**
     * @return string
     */
    public function getUserFullname(): string
    {
        return $this->user_fullname;
    }

    /**
     * @param string $user_fullname
     */
    public function setUserFullname(string $user_fullname): void
    {
        $this->user_fullname = $user_fullname;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->user_username;
    }

    /**
     * @param string $user_username
     */
    public function setUsername(string $user_username): void
    {
        $this->user_username = $user_username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->user_password;
    }

    /**
     * @param string $user_password
     */
    public function setPassword(string $user_password): void
    {
        $this->user_password = $user_password;
    }

    /**
     * @return \DateTime
     */
    public function getUserCreationdate(): \DateTime
    {
        return $this->user_creationdate;
    }

    /**
     * @param \DateTime $user_creationdate
     */
    public function setUserCreationdate(\DateTime $user_creationdate): void
    {
        $this->user_creationdate = $user_creationdate;
    }

    /**
     * @return int
     */
    public function getUserSumUpVotes(): int
    {
        return $this->user_sumUpVotes;
    }

    /**
     * @param int $user_sumUpVotes
     */
    public function setUserSumUpVotes(int $user_sumUpVotes): void
    {
        $this->user_sumUpVotes = $user_sumUpVotes;
    }

    /**
     * @return int
     */
    public function getUserSumDownVotes(): int
    {
        return $this->user_sumDownVotes;
    }

    /**
     * @param int $user_sumDownVotes
     */
    public function setUserSumDownVotes(int $user_sumDownVotes): void
    {
        $this->user_sumDownVotes = $user_sumDownVotes;
    }

    /**
     * @return Collection|Feedback[]
     */
    public function getUserFeedbacks(): Collection
    {
        return $this->user_feedbacks;
    }

    /**
     * @return Collection|Document[]
     */
    public function getUserDocuments(): Collection
    {
        return $this->user_documents;
    }


    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->user_permissions;
    }

    /**
     * @param string[] $user_permissions
     */
    public function setRoles(array $user_permissions): void
    {
        $this->user_permissions = $user_permissions;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
    }
}