<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Document
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="documents")
 */

class Document
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $document_id = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $document_title = "";

    /**
     * @var string
     * @ORM\Column(type="string", length=2000, nullable=false)
     */
    private $document_content = "";

    /**
     * @var User|null
     * @ORM\JoinColumn(name="document_creator", referencedColumnName="user_id")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="user_documents")
     */
    private $document_creator;

    /**
     * @var Topic|null
     * @ORM\JoinColumn(name="document_topic", referencedColumnName="topic_id")
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="topic_documents")
     */
    private $document_topic;

    /**
     * @var string
     * @ORM\Column(type="string", length=500, nullable=false)
     */
    private $document_description = "";


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $document_creationdate;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $document_likes = 0;


    /**
     * @var Collection|Feedback[]
     * @ORM\OneToMany(targetEntity="Feedback", mappedBy="feedback_document")
     */
    private $document_feedbacks;


    public function __construct()
    {
        $this->document_feedbacks = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getDocumentTitle(): string
    {
        return $this->document_title;
    }

    /**
     * @param string $document_title
     */
    public function setDocumentTitle(string $document_title): void
    {
        $this->document_title = $document_title;
    }

    /**
     * @return string
     */
    public function getDocumentContent(): string
    {
        return $this->document_content;
    }

    /**
     * @param string $document_content
     */
    public function setDocumentContent(string $document_content): void
    {
        $this->document_content = $document_content;
    }

    /**
     * @return User|null
     */
    public function getDocumentCreator(): ?User
    {
        return $this->document_creator;
    }

    /**
     * @param User|null $document_creator
     */
    public function setDocumentCreator(?User $document_creator): void
    {
        $this->document_creator = $document_creator;
    }

    /**
     * @return Topic|null
     */
    public function getDocumentTopic(): ?Topic
    {
        return $this->document_topic;
    }

    /**
     * @param Topic|null $document_topic
     */
    public function setDocumentTopic(?Topic $document_topic): void
    {
        $this->document_topic = $document_topic;
    }

    /**
     * @return string
     */
    public function getDocumentDescription(): string
    {
        return $this->document_description;
    }

    /**
     * @param string $document_description
     */
    public function setDocumentDescription(string $document_description): void
    {
        $this->document_description = $document_description;
    }

    /**
     * @return \DateTime
     */
    public function getDocumentCreationdate(): \DateTime
    {
        return $this->document_creationdate;
    }

    /**
     * @param \DateTime $document_creationdate
     */
    public function setDocumentCreationdate(\DateTime $document_creationdate): void
    {
        $this->document_creationdate = $document_creationdate;
    }

    /**
     * @return integer
     */
    public function getDocumentLikes()
    {
        return $this->document_likes;
    }

    /**
     * @param integer $document_likes
     */
    public function setDocumentLikes($document_likes): void
    {
        $this->document_likes = $document_likes;
    }

    /**
     * @return int
     */
    public function getDocumentId(): int
    {
        return $this->document_id;
    }

    /**
     * @return Feedback[]|Collection
     */
    public function getDocumentFeedbacks()
    {
        return $this->document_feedbacks;
    }
}