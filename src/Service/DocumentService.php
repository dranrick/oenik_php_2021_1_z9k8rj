<?php

namespace App\Service;

use App\DTO\DocumentDTO;
use App\Entity\Topic;
use App\Entity\Feedback;
use App\Entity\Document;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PhpParser\Comment\Doc;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Form\FormFactoryInterface;

class DocumentService extends CrudService
{
    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getDocumentById(int $id) : Document
    {
        return $this->getRepository()->find($id);
    }

    public function getDocumentsByUserId(int $user_id) : iterable
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($user_id);
        return $user->getUserDocuments();
    }

    public function getAllDocuments() : iterable
    {
        return $this->getRepository()->findAll();
    }

    public function addDocument(string $title, User $creator, string$description, string $content, int $topicId) : void
    {
        $document = new Document();

        $document->setDocumentTitle($title);
        $document->setDocumentCreator($creator);
        $document->setDocumentDescription($description);
        $document->setDocumentContent($content);
        $document->setDocumentLikes(0);
        $document->setDocumentCreationdate(new \DateTime());

        /** @var Topic $topic */
        $topic = $this->em->getRepository(Topic::class)->find($topicId);

        $currentTopicCount = $topic->getTopicDocumentCount();
        $topic->setTopicDocumentCount($currentTopicCount + 1);

        $document->setDocumentTopic($topic);

        $this->em->persist($document);
        $this->em->flush();
    }

    public function editDocument(int $document_id, string $title, string $description, string $content) : void
    {
        $documentToEdit = $this->getDocumentById($document_id);

        $documentToEdit->setDocumentTitle($title);
        $documentToEdit->setDocumentDescription($description);
        $documentToEdit->setDocumentContent($content);

        $this->em->persist($documentToEdit);
        $this->em->flush();
    }

    public function deleteDocument(int $document_id) : void
    {
        $documentToDelete = $this->getDocumentById($document_id);

        $feedbacks = $documentToDelete->getDocumentFeedbacks();
        foreach ($feedbacks as $feedbackToDelete)
        {
            $this->em->remove($feedbackToDelete);
            $this->em->flush();
        }

        /** @var Topic $topicAffected */
        $topicAffected = $documentToDelete->getDocumentTopic();
        $topicAffected->setTopicDocumentCount($topicAffected->getTopicDocumentCount() - 1);

        $this->em->remove($documentToDelete);
        $this->em->flush();
    }

    public function likeDocument(int $document_id) : void
    {
        /** @var Document $documentToLike */
        $documentToLike = $this->em->getRepository(Document::class)->find($document_id);

        $noCurrentLikes = $documentToLike->getDocumentLikes();
        $documentToLike->setDocumentLikes($noCurrentLikes + 1);

        $this->em->persist($documentToLike);
        $this->em->flush();
    }

    public function getRepository(): EntityRepository
    {
        return $this->em->getRepository(Document::class);
    }
}