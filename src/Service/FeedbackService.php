<?php


namespace App\Service;

use App\Entity\Topic;
use App\Entity\Feedback;
use App\Entity\User;
use App\Entity\Document;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormFactoryInterface;

class FeedbackService extends CrudService
{

    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory)
    {
        parent::__construct($em, $formFactory);
    }

    public function getFeedbackById(int $feedback_id) : Feedback
    {
        /** @var Feedback $feedback*/
        $feedback = $this->getRepository()->find($feedback_id);;
        return $feedback;
    }

    public function addFeedback(int $documentId, User $author, string $text) : void
    {
        $feedback = new Feedback();

        $feedback->setFeedbackText($text);
        $feedback->setFeedbackAuthor($author);
        $feedback->setFeedbackUpvotes(0);
        $feedback->setFeedbackDownvotes(0);
        $feedback->setFeedbackDate(new \DateTime());

        /** @var Document $document*/
        $document = $this->em->getRepository(Document::class)->find($documentId);
        $feedback->setFeedbackDocument($document);

        $this->em->persist($feedback);
        $this->em->persist($document);
        $this->em->persist($author);

        $this->em->flush();
    }

    public function editFeedback(int $feedback_id, string $feedback_text) : void
    {
        /** @var Feedback $feedbackToEdit */
        $feedbackToEdit = $this->getFeedbackById($feedback_id);
        $feedbackToEdit->setFeedbackText($feedback_text);

        $this->em->persist($feedbackToEdit);
        $this->em->flush();
    }

    public function deleteFeedback(int $feedback_id) : void
    {
        /** @var Feedback $feedbackToDelete */
        $feedbackToDelete = $this->em->getRepository(Feedback::class)->find($feedback_id);

        $this->em->persist($feedbackToDelete);
        $this->em->flush();
    }

    public function voteOnFeedback(int $feedback_id, int $upordown) : void
    {
        /** @var Feedback $feedbackToVoteOn */
        $feedbackToVoteOn = $this->em->getRepository(Feedback::class)->find($feedback_id);

        /** @var User $user */
        $user = $feedbackToVoteOn->getFeedbackAuthor();

        if ($upordown == 0)
        {
            $currentDownVoteCount = $feedbackToVoteOn->getFeedbackDownvotes();
            $feedbackToVoteOn->setFeedbackDownvotes($currentDownVoteCount + 1);
            $user->setUserSumDownVotes($user->getUserSumDownVotes() + 1);
        }
        else
        {
            $currentUpVoteCount = $feedbackToVoteOn->getFeedbackUpvotes();
            $feedbackToVoteOn->setFeedbackUpvotes($currentUpVoteCount + 1);
            $user->setUserSumUpVotes($user->getUserSumUpVotes() + 1);
        }

        $this->em->persist($feedbackToVoteOn);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function getPopularFeedbacks(int $threshold) : iterable
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select("feedback")
            ->where("feedback.feedback_upvotes > :threshold")
            ->orderBy("feedback.feedback_upvotes")
            ->setParameter("threshold", $threshold);
        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getRepository(): EntityRepository
    {
        /** @var EntityRepository $repository*/
        $repository = $this->em->getRepository(Feedback::class);
        return $repository;
    }
}