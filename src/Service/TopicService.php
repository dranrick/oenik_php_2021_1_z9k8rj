<?php

namespace App\Service;

use App\Entity\Topic;
use App\Entity\Document;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use phpDocumentor\Reflection\Types\Array_;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TopicService extends CrudService
{
    /** @var DocumentService  */
    private $documentService;

    public function __construct(EntityManagerInterface $em, FormFactoryInterface $formFactory, DocumentService $documentService)
    {
        parent::__construct($em, $formFactory);
        $this->documentService = $documentService;
    }

    public function getAllTopics() : iterable
    {
        return $this->getRepository()->findAll();
    }

    public function getTopicById(int $id) : Topic
    {
        /** @var Topic $topic */
        return $this->getRepository()->find($id);
    }

    public function getIdByTitle(string $topic_name) : int
    {
        echo $topic_name;
        /** @var Topic $topic */
        $topic = $this->getRepository()->findOneBy((['topic_name' => $topic_name]));
        return $topic->getTopicId();
    }

    public function getTopicDocuments(int $id) : iterable
    {
        /** @var Topic $topic */
        $topic = $this->getRepository()->find($id);
        return $topic->getTopicDocuments();
    }

    public function addTopic(string $name, string $description): void
    {
        $topic = new Topic();

        $topic->setTopicName($name);
        $topic->setTopicDescription($description);
        $topic->setTopicDocumentCount(0);
        $topic->setTopicCreationdate(new \DateTime());

        $this->em->persist($topic);
        $this->em->flush();
    }

    public function editTopic(int $topic_id, string $topic_name, string $topic_description): void
    {
        /** @var Topic $topicToEdit */
        $topicToEdit = $this->getTopicById($topic_id);

        $topicToEdit->setTopicName($topic_name);
        $topicToEdit->setTopicDescription($topic_description);

        $this->em->persist($topicToEdit);
        $this->em->flush();
    }

    public function deleteTopic(int $topicId) : void
    {
        /** @var Topic $topicToDelete */
        $topicToDelete = $this->em->getRepository(Topic::class)->find($topicId);

        /** @var Document[] $documents */
        $documents = $topicToDelete->getTopicDocuments();

        foreach ($documents as $documentToDelete)
        {
            $document_id = $documentToDelete->getDocumentId();
            $this->documentService->deleteDocument($document_id);
        }

        $this->em->remove($topicToDelete);
        $this->em->flush();
    }

    public function mostAvgLikedTopic() : iterable
    {
        $documents = $this->documentService->getAllDocuments();

        /** @var Topic[] $topics */
        $topics = $this->getAllTopics();

        $result = array();

        foreach ($topics as $topic)
        {
            $result[$topic->getTopicName()] = $this->averageTopicLikes($topic->getTopicId());
        }

        return $result;
        $mostLiked = array_search(max($result), $result);
    }

    public function averageTopicLikes($topic_id) : float
    {
        $topic = $this->getTopicById($topic_id);
        $avg = 0.0;

        $documents = $topic->getTopicDocuments();

        foreach ($documents as $document)
        {
            $avg += $document->getDocumentLikes();
        }
        return $avg / $topic->getTopicDocumentCount();
    }

    public function getRepository(): EntityRepository
    {
        return $this->em->getRepository(Topic::class);
    }
}