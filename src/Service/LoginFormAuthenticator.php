<?php

namespace App\Service;

use App\DTO\LoginDTO;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\Security\Core\authentication\Token\TokenInterface;
//use Symfony\Component\Security\Core\Exception\AuthenticationException;
//use Symfony\Component\Security\Core\User\UserInterface;
//use Symfony\Component\Security\Core\User\UserProviderInterface;
//use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
//use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait; // For redirecting...

    /** @var EntityManagerInterface  */
    private $em;

    /** @var RouterInterface  */
    private $router;

    /** @var FormFactoryInterface  */
    private $formFactory;

    /** @var UserPasswordEncoderInterface  */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $em, RouterInterface $router, UserPasswordEncoderInterface $encoder, FormFactoryInterface $ff)
    {
        $this->em = $em;
        $this->router = $router;
        $this->passwordEncoder = $encoder;
        $this->formFactory = $ff;
    }

    protected function getLoginUrl()
    {
        return $this->router->generate("app_login");
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'app_login' && $request->isMethod('POST'); //Decides whether the autheticator can work according to the current circumstances
        //Only works if the request goes to the app_login route and it is a POST request
    }

    public function getCredentials(Request $request)
    {
        $dto = new LoginDTO($this->formFactory, $request);
        $form = $dto->getForm();
        $form->handleRequest($request);

        if (!$form->isValid() || !$form->isSubmitted())
        {
            throw new InvalidCsrfTokenException("The form is invalid");
        }

        $request->getSession()->set(Security::LAST_USERNAME, $dto->getUserName()); //Getting last username with the help of the LAST_USERNAME attr.

        return $dto;
    }


    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var LoginDTO $credentials */
        $user = $this->em->getRepository(User::class)->findOneBy(['user_username'=>$credentials->getUserName()]);

        if(!$user)
        {
            throw new CustomUserMessageAuthenticationException("Bad username");
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        /** @var LoginDTO $credentials */
        return $this->passwordEncoder->isPasswordValid($user, $credentials->getUserPass());
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $targetPath = $this->getTargetPath($request->getSession(), $providerKey);

        if ($targetPath)
        {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->router->generate("app_login"));
    }
}