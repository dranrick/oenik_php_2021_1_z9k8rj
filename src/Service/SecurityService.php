<?php

namespace App\Service;

use App\Entity\Document;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityService
{
    /** @var EntityManagerInterface  */
    private $em;
    /** @var UserPasswordEncoderInterface  */
    private $encoder;

    /** @var DocumentService */
    private $documentService;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, DocumentService $documentService)
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->documentService = $documentService;
    }

    public function getUserById(int $user_id) : User
    {
        /** @var User $user */
        $user = $this->em->getRepository(User::class)->find($user_id);

        return $user;
    }

    public function getAllUsers(): iterable
    {
        return $this->em->getRepository(User::class)->findAll();
    }

    public function registerUser(string $username, string $fullname , string $clearPass) : void
    {
        $user = new User();
        $user->setUserFullname($fullname);
        $user->setUsername($username);
        $user->setRoles(["ROLE_USER"]);
        $user->setUserCreationdate(new \DateTime());
        $user->setPassword($this->encoder->encodePassword($user, $clearPass)); //hashing the password
        $this->em->persist($user); //only saves to entitymanager
        $this->em->flush(); //saves to db
    }

    public function editUser(int $user_id, string $username, string $fullname, string $clearpass)
    {
        /** @var User $userToEdit */
        $userToEdit = $this->em->getRepository(User::class)->find($user_id);

        $userToEdit->setUsername($username);
        $userToEdit->setUserFullname($fullname);

        if ($clearpass && $clearpass != "")
        {
            $encoded_pw = $this->encoder->encodePassword($userToEdit, $clearpass);
            $userToEdit->setPassword($encoded_pw);
        }

        $this->em->persist($userToEdit);
        $this->em->flush();
    }

    public function deleteUser(int $user_id) : void
    {
        /** @var User $userToDelete */
        $userToDelete = $this->em->getRepository(User::class)->find($user_id);

        $documents = $userToDelete->getUserDocuments();
        foreach ($documents as $documentToDelete)
        {
            $document_id = $documentToDelete->getDocumentId();
            $this->documentService->deleteDocument($document_id);
        }

//        $feedbacks = $userToDelete->getUserFeedbacks();
//        foreach ($feedbacks as $feedbackToDelete)
//        {
//            $f
//        }

        $this->em->remove($userToDelete);
        $this->em->flush();
    }


    public function checkPassword(string $username, string $clearPass) : bool
    {
        $user = $this->em->getRepository(User::class)->findOneBy(["username"=>$username]);
        if (!$user)
        {
            return false;
        }

        return $this->encoder->isPasswordValid($user, $clearPass);
    }

    public function changeRole($user_id) : void
    {
        $user = $this->getUserById($user_id);
        $currentrole = $user->getRoles();

        if ($currentrole[0] == "ROLE_USER")
        {
            $currentrole[0] = "ROLE_ADMIN";
        }
        else
        {
            $currentrole[0] = "ROLE_USER";
        }

        $user->setRoles($currentrole);

        $this->em->persist($user);
        $this->em->flush();
    }
}